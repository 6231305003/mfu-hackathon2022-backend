// Import module
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const routes = require("./routes/index")

// Init Express and Module
const app = express();
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false, limit: '50MB' }))
app.use(bodyParser.json({ limit: '50MB' }));
dotenv.config();

// Define a Port 
const PORT = process.env.HOST || 8000;

//Connect Database
mongoose.connect(process.env.MONGO_HOST,{
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

//Show error when connection problems
db.on('error', (err) => console.error(err));
//Once Connected Database
db.once('open',() => console.log(`Connected Database at : ${db.host}`));


//Declare Function
const timeNow = (req, res, next) => {
  console.log("Path Detects:", req.url);
  next();
};

//Initialize Function
app.use(timeNow);


//Setup Header
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

app.use(routes)

//Listen a port
app.listen(PORT, () => {
  console.log(`Server Started ON PORT : ${PORT}`);
});
