const mongoose = require("mongoose");

const studentSchema = new mongoose.Schema({
    studentId : {
        type : String,
        required : true,
        unique : true
    },
    firstName : {
        type : String,
        required : true,
    },
    lastName : {
        type : String,
        required : true,
    },
    major : {
        type : mongoose.Schema.ObjectId,
        ref : 'Major'
    },
    createdAt : {
        type : Date,
        default : Date.now
    }
})

module.exports = mongoose.model('Student',studentSchema)