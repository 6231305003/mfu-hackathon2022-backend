const express = require('express');
const router = express.Router();
const MajorModel = require("../models/major")

// @desc Getting all major
// @route GET /majors
router.get('/',async (req,res) => {
    try {
        const major = await MajorModel.find();
        console.log("Get All Majors Successful")
        res.status(200).json({data : major})
    }catch (err){
        console.error(err.message)
        res.status(500).json({message : err.message})
        
    }
});

// @desc Getting by major Id
// @route GET /majors/:id
router.get('/:id',getMajor,async (req,res) => {
    console.log("Get Majors by ID Successful")
    res.status(200).json({data : req.major})
});

// @desc Creating one major
// @route POST /majors/:id
router.post('/',async (req,res) => {
    try {
        const newMajor = await MajorModel.create(req.body)
        console.log("Create Majors Successful")
        res.status(201).json({data : newMajor})
    }catch (err){
        if(err.code === 11000){
            const keys = Object.keys(err.keyValue);
            err.message = `This ${keys.toString()} is already taken.`
        }
        console.error(err.message)
        res.status(500).json({message : err.message})
    }
})

// @desc Updating one major
// @route PUT /majors/:id
router.put('/:id',async (req,res) => {
    const majorID = req.params.id
    try {
        const updatedMajor = await MajorModel.findByIdAndUpdate(majorID,req.body,{
            new : true,
            runValidators : true
        });
        console.log("Update Majors Successful")
        res.json({data : updatedMajor})
    }catch (err){
        console.error(err.message)
        res.status(400).json({message : err.message})
    }
})

// @desc Deleting one major
// @route DELETE /majors/:id
router.delete('/:id',getMajor,async (req,res) => {
    const majorRemove = req.major
    try {
        await majorRemove.remove();
        console.log("Delete Majors Successful")
        res.json({message : "Delete Major Successful",data : {}})
    }catch (err){
        res.status(500).json({message : err.message})
    }
})

//Validate Major(Middleware Functions)
async function getMajor(req,res,next) {
    let major;
    try{
        major = await MajorModel.findById(req.params.id);
        if (!major){
            return res.status(404).json({message : "Cannot Find Major"})
        }
    }catch (err){
        //Bad ObjectID
        let message = err.message;
        let code = 500;
        if(err.name = "CastError"){
            message = "Data Not Found";
            code = 400;
        }
        console.error(err.message)
        return res.status(code).json({message : message})
    }
    req.major = major;
    next();
}

module.exports = router;