const express = require('express');
const router = express.Router();
const StudentModel = require("../models/student")

// @desc Getting all students
// @route GET /students
router.get('/',async (req,res) => {
    try {
        const students = await StudentModel.find().populate({path : 'major'});
        console.log("Get All Students Successful")
        res.status(200).json({data : students})
    }catch (err){
        console.error(err.message)
        res.status(500).json({message : err.message})
    }
});

// @desc Getting by student Id
// @route GET /students/:id
router.get('/:id',getStudent,async (req,res) => {
    console.log("Get Students by ID Successful")
    res.status(200).json({data : req.student})
});

// @desc Creating one student
// @route POST /students/:id
router.post('/',async (req,res) => {
    try {
        const newStudents = await StudentModel.create(req.body);
        console.log("Create Students Successful")
        res.status(201).json({data : newStudents})
    }catch (err){
        if(err.code === 11000){
            const keys = Object.keys(err.keyValue);
            err.message = `This ${keys.toString()} is already taken.`
        }
        console.error(err.message)
        res.status(500).json({message : err.message})
    }
})

// @desc Updating one student
// @route PUT /students/:id
router.put('/:id',async (req,res) => {
    const stuID = req.params.id
    try {
        const updatedStudents = await StudentModel.findByIdAndUpdate(stuID,req.body,{
            new : true,
            runValidators : true
        });
        console.log("Update Students Successful")
        res.json({data : updatedStudents})
    }catch (err){
        console.error(err.message)
        res.status(400).json({message : err.message})
    }
})

// @desc Deleting one student
// @route DELETE /students/:id
router.delete('/:id',getStudent,async (req,res) => {
    const stuRemove = req.student
    try {
        await stuRemove.remove();
        console.log("Delete Students Successful")
        res.json({message : "Delete Student Successful",data : {}})
    }catch (err){
        console.error(err.message)
        res.status(500).json({message : err.message})
    }
})

//Validate Student ID (Middleware Functions)
async function getStudent(req,res,next) {
    let student;
    try{
        student = await StudentModel.findById(req.params.id).populate({path : 'major'});
        if (!student){
            return res.status(404).json({message : "Cannot Find Student"})
        }
    }catch (err){
        //Bad ObjectID
        let message = err.message;
        let code = 500;
        if(err.name = "CastError"){
            message = "Data Not Found";
            code = 400;
        }
        console.error(err.message)
        return res.status(code).json({message : message})

    }
    req.student = student;
    next();
}

module.exports = router;